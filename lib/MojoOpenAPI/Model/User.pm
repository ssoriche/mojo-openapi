package MojoOpenAPI::Model::User;

use v5.24;
use Moo;
use YAML::XS qw(LoadFile);
use Crypt::Eksblowfish::Bcrypt qw(bcrypt en_base64);

has 'username' => ( is => 'ro', );
has 'password' => ( is => 'ro', );

has 'db' => (
  is      => 'ro',
  default => 'db/users.yml',
);

sub _bcrypt {
  my ( $self, $password, $settings ) = @_;
  $password //= $self->password;
  unless ( defined $settings && $settings =~ /^\$2a\$/ ) {
    my $cost = sprintf( '%02d', 6 );
    $settings = join( '$', '$2a', $cost, _salt() );
  }
  return bcrypt( $password, $settings );
}

sub validate {
  my ( $self, $plain ) = @_;

  my $users = LoadFile($self->db);

  return 0 unless $self->username && $users && exists $users->{$self->username};
  my $crypted = $users->{$self->username};

  return $self->_bcrypt( $plain, $crypted ) eq $crypted;
}

sub _salt {
  my $num = 999999;
  my $cr  = crypt( rand($num), rand($num) ) . crypt( rand($num), rand($num) );
  en_base64( substr( $cr, 4, 16 ) );
}

1;
