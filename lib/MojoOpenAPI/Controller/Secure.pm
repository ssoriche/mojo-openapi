package MojoOpenAPI::Controller::Secure;

use Mojo::Base 'Mojolicious::Controller';

sub access {
  my $c = shift;
  return unless $c->openapi->valid_input;

  my $args = $c->validation->output;
  my $token = $c->req->headers->authorization;
  my $username = $c->stash('username');

  $c->render(json => { username => $username, args => $args });
}

1;
