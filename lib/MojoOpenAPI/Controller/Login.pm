package MojoOpenAPI::Controller::Login;

use Mojo::Base 'Mojolicious::Controller';
use MojoOpenAPI::Model::User;


sub login {
  my $c = shift;
  return unless $c->openapi->valid_input;

  my $args = $c->validation->output;
  my $user = MojoOpenAPI::Model::User->new(
    username => $args->{username},
    password => $args->{password},
    db => $c->config->{user_db},
  );

  return $c->rendered(403) unless $user->validate;

  my $jwt = Mojo::JWT->new(secret => $c->app->secrets->[0] || die);
  $jwt->claims({ username => $args->{username}, exp => time + 24 * 60 * 60  });

  $c->render(json => { token => $jwt->encode });
}

1;
