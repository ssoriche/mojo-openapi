package MojoOpenAPI;

use Mojo::Base 'Mojolicious';

use Config::ZOMG();
use Try::Tiny qw( catch try );
use Mojo::JWT;

sub startup {
  my $self = shift;

  $self->config(
    Config::ZOMG->new(
      name => 'mojoopenapi',
      path => $self->home->to_string,
    )->load
  );

  $self->_set_up_routes;
}

sub _set_up_routes {
  my $self = shift;

  $self->plugin( 'Bcrypt' );
  $self->plugin( 'OpenAPI' => {
      url => $self->home->rel_file('public/v1.yml'),
      security => {
        default => sub {
          my ($c, $definition, $scopes, $cb) = @_;
          return $c->$cb('Authorization header not present') unless $c->req->headers->authorization;

          my $token = $c->req->headers->authorization;

          my $jwt = Mojo::JWT->new(secret => $c->app->secrets->[0]);
          try {
            $jwt->decode($token);
          }
          catch {
            return $c->$cb('403')
          };

          $c->stash( username => $jwt->claims->{username} );
          return $c->$cb();
        }
      }
    } );
}

1;
