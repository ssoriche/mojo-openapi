use Mojo::Base -strict;

use Test::Mojo;
use Test::More;

my $t = Test::Mojo->new('MojoOpenAPI');

# Post a JSON document
# $t->post_ok('/notifications' => json => {event => 'full moon'})
#   ->status_is(201)
#   ->json_is('/message' => 'notification created');

# Perform GET requests and look at the responses
$t->get_ok('/index.html')
  ->status_is(200)
  ->content_like(qr/redoc spec-url/);

done_testing;
