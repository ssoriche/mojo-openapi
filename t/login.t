use Mojo::Base -strict;

use Test::Mojo;
use Test::More;
use diagnostics;

my $t = Test::Mojo->new('MojoOpenAPI' => { user_db => 't/db/users.yml' });

# Post a JSON document
$t->post_ok('/v1/login' => form => {username => 'user01', password => 'test_password'})
  ->status_is(200)
  ->json_has('/token')
;

$t->post_ok('/v1/login' => form => {username => 'user01', password => 'not_test_password'})
  ->status_is(403)
;

$t->post_ok('/v1/login' => form => {username => 'wrong_user', password => 'not_test_password'})
  ->status_is(403)
;

$t->post_ok('/v1/login' => form => {username => 'wrong_user', password => 'test_password'})
  ->status_is(403)
;

done_testing;
