use Mojo::Base -strict;

use Test::Mojo;
use Test::More;
use diagnostics;

my $t = Test::Mojo->new('MojoOpenAPI' => { user_db => 't/db/users.yml' });

# Post a JSON document
$t->post_ok('/v1/login' => form => {username => 'user01', password => 'test_password'})
  ->status_is(200)
  ->json_has('/token')
;

my $token = $t->tx->res->json->{token};

$t->get_ok('/v1/secure' => {'Authorization' => $token})
  ->status_is(200)
  ->json_is('/username' => 'user01')
;

$t->get_ok('/v1/secure' => {'Authorization' => "${token}1"})
  ->status_is(401)
;

$t->get_ok('/v1/secure' => {'Authorization' => "NOTAVALIDTOKEN"})
  ->status_is(401)
;

done_testing;
