use Mojo::Base -strict;

use Test::Mojo;
use Test::More;
use diagnostics;

my $t = Test::Mojo->new('MojoOpenAPI' => { user_db => 't/db/users.yml' });

# Post a JSON document
$t->post_ok('/v1/login' => form => {username => 'user01', password => 'test_password'})
  ->status_is(200)
  ->json_has('/token')
;

my $token = $t->tx->res->json->{token};
my $jwt = Mojo::JWT->new(secret => $t->app->secrets->[0] || die);

is $jwt->decode($token)->{username} => 'user01', 'encoded username matches';

done_testing;
