use Test::More;
use MojoOpenAPI::Model::User;

my $user = MojoOpenAPI::Model::User->new(username => 'test', db => 't/db/users.yml');
isa_ok($user, 'MojoOpenAPI::Model::User');

ok !$user->validate('test_password'), 'invalid user -- valid password';

$user = MojoOpenAPI::Model::User->new(username => 'user01', db => 't/db/users.yml');
ok !$user->validate('test'), 'valid user -- invalid password';
ok $user->validate('test_password'), 'valid user -- valid password';

done_testing();
