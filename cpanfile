requires 'Config::ZOMG';
requires 'Mojo::JWT';
requires 'Mojolicious';
requires 'Mojolicious::Plugin::Bcrypt';
requires 'Mojolicious::Plugin::OpenAPI';
requires 'YAML';
requires 'YAML::XS';

requires 'Try::Tiny';
